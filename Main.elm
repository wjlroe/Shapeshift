import Char exposing (KeyCode)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import Signal
import Time exposing (..)
import Window

type alias Direction = { x : KeyCode, y: KeyCode }
type alias Delta = Float
type alias Dimentions = (Int, Int)
type alias InputSignals = (Delta, Direction, Dimentions)

type alias Position a = { a | x:Int, y:Int }
type alias Coordinates = { x:Float, y: Float }
type alias Velocity a = { a | vx:Float, vy:Float }
type alias Object a = { a | x:Float, y:Float, vx:Float, vy:Float }
type alias Size a = { a | width:Int, height:Int }
type alias Sprite = { spriteNum:Int, animTime:Float, spriteSize:Int, rowSize:Int, spriteCount:Int, dtPerFrame:Float }
type alias HasSprite a = { a | sprite:Sprite }
type alias Runner = HasSprite (Position {})
type alias HasRunner a = { a | runner:Runner }
type alias World = Size (HasRunner {})

runnerSprite : Sprite
runnerSprite = { spriteNum = 0, animTime = 0, spriteSize = 125, rowSize = 4, spriteCount = 16, dtPerFrame = 40 }

runner : Runner
runner = { x = 0, y = 0, sprite = runnerSprite }

world : World
world = { width = 5, height = 10, runner = runner }

input : Signal InputSignals
input = let delta = Signal.map inMilliseconds (fps 60)
            directions = Signal.merge Keyboard.arrows Keyboard.wasd
            inputSignals = Signal.map3 (,,) delta directions Window.dimensions
        in Signal.sampleOn delta inputSignals

spriteIndex : Sprite -> Int -> (Int, Int)
spriteIndex sprite num = let row = num // sprite.rowSize
                             col = num % sprite.rowSize
                         in (col * sprite.spriteSize, row * sprite.spriteSize)

draw : Dimentions -> World -> Element
draw (w', h') world =
    let (w, h) = (toFloat w', toFloat h')
        runnerImg = "assets/imgs/spritesheet.png"
        runnerSprite = world.runner.sprite
    in collage w' h'
       [ toForm (croppedImage (spriteIndex runnerSprite runnerSprite.spriteNum)
                              runnerSprite.spriteSize
                              runnerSprite.spriteSize
                              runnerImg)
       |> scale 4.0
       |> move (toFloat world.runner.x, toFloat world.runner.y)
       ]

animate : Delta -> World -> World
animate dt world =
  let oldrunner = world.runner
      oldRunnerSprite = oldrunner.sprite
      newAnimTime = oldRunnerSprite.animTime + dt
      newFrame = newAnimTime >= oldRunnerSprite.dtPerFrame
      nextSpriteNum = if newFrame then ((oldRunnerSprite.spriteNum + 1) % oldRunnerSprite.spriteCount) else oldRunnerSprite.spriteNum
      nextAnimTime = if newFrame then clamp 0 oldRunnerSprite.dtPerFrame (newAnimTime - oldRunnerSprite.dtPerFrame) else newAnimTime
      newSprite = { oldRunnerSprite | spriteNum = nextSpriteNum, animTime = nextAnimTime }
  in { world | runner = { oldrunner | sprite = newSprite } }

worldSize : Dimentions -> World -> World
worldSize (w,h) world = world
-- worldSize (w,h) world = let mHeight = h // world.height
--                             mWidth  = w // world.width
--                             sqSize = min mHeight mWidth
--                         in watch "world" { world | sqSize = sqSize,
--                                            rSizeF = sqSize // 2 }

step : (Delta, Direction, Dimentions) -> World -> World
step (dt, keys, wh) = worldSize wh >> animate dt

main : Signal Element
main = Signal.map2 draw Window.dimensions (Signal.foldp step world input)
