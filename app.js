var game = function(spec) {
  var that = {currentFrame: 0,
              counter: 0};

  console.debug(spec);
  that.canvas = document.getElementById(spec.id);
  // that.imageSmoothingEnabled = false;
  that.ctx = that.canvas.getContext('2d');

  that.image = new Image();
  that.destWidth = spec.frameWidth * spec.scale;
  that.destHeight = spec.frameHeight * spec.scale;

  // calculate the number of frames in a row after the image loads
  that.image.onload = function() {
    that.framesPerRow = Math.floor(that.image.width / spec.frameWidth);
  };

  that.image.src = spec.path;

  that.update = function() {
    // update to the next frame if it is time
    if (that.counter == (spec.frameSpeed - 1)) {
      that.currentFrame = (that.currentFrame + 1) % spec.endFrame;
    }

    // update the counter
    that.counter = (that.counter + 1) % spec.frameSpeed;
  };

  that.draw = function() {
    var row = Math.floor(that.currentFrame / that.framesPerRow);
    var col = Math.floor(that.currentFrame % that.framesPerRow);

    that.ctx.drawImage(
      that.image,
      col * spec.frameWidth,
      row * spec.frameHeight,
      spec.frameWidth,
      spec.frameHeight,
      that.x, that.y,
      that.destWidth,
      that.destHeight
    );
  };

  that.resize_canvas = function() {
    that.canvas.width = window.innerWidth;
    that.canvas.height = window.innerHeight;

    that.x = that.canvas.width / 2 - that.destWidth / 2;
    that.y = that.canvas.height / 2 - that.destHeight / 2;
  };

  that.start = function() {
    requestAnimationFrame(that.start);
    that.ctx.clearRect(0, 0, 150, 150);
    that.update();
    that.draw();
  };

  that.resize_canvas();

  return that;
}

var the_game = game({id: 'canvas',
                     scale: 4.0,
                     path: "assets/imgs/spritesheet.png",
                     frameWidth: 125,
                     frameHeight: 125,
                     frameSpeed: 3,
                     startFrame: 0,
                     endFrame: 15});

// for debugging in the console...
window.the_game = the_game;
window.addEventListener('resize', the_game.resize_canvas, false);
the_game.start();
